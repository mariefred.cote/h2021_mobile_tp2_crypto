package com.h2021.tp2_crypto.data;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.h2021.tp2_crypto.model.Currency;

@Database(entities = {Currency.class}, version = 1)
public abstract class CurrencyRoomDatabase extends RoomDatabase {

    public static volatile CurrencyRoomDatabase INSTANCE;
    public abstract CurrencyDao currencyDao();

    public static synchronized CurrencyRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),CurrencyRoomDatabase.class, "currency_table").build();
        }
        return INSTANCE;
    }
}
