package com.h2021.tp2_crypto.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.h2021.tp2_crypto.model.Currency;

import java.util.List;

@Dao
public interface CurrencyDao {

    @Insert
    void insert(Currency currency);

    @Query("DELETE FROM currency_table")
    void deleteAll();

    @Query("SELECT * FROM currency_table")
    LiveData<List<Currency>> getAllCurrency();

    @Query("SELECT * FROM currency_table")
    List<Currency> getCurrencies();

    @Query("UPDATE currency_table SET portefeuille_col= :portefeuille WHERE id= :id")
    void updatePortefeuille(Boolean portefeuille, int id);

    @Query("SELECT * FROM currency_table WHERE portefeuille_col= :portefeuille")
    LiveData<List<Currency>> getAllWallet(Boolean portefeuille);

    @Query("UPDATE currency_table SET nom_col= :nom, symbole_col= :symbole, prix_col= :prix, quantite_col= :quantite, total_col= :total WHERE id= :id ")
    void updateCurrency(String nom, String symbole, long prix, long quantite, long total, int id);

    @Query("DELETE FROM currency_table WHERE id= :id")
    void deleteCurrency(int id);


}
