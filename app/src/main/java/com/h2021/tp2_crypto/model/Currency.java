package com.h2021.tp2_crypto.model;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "currency_table")
public class Currency  {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nom_col" )
    private String nom;


    //@NonNull
    @ColumnInfo(name = "symbole_col" )
    private String symbole;

    //@NonNull
    @ColumnInfo(name = "prix_col" )
    private long prix;

    @ColumnInfo(name = "quantite_col" )
    private long quantite;

    @ColumnInfo(name = "total_col" )
    private long total;

    //@NonNull
    @ColumnInfo(name = "portefeuille_col" )
    private boolean portefeuille;


    public Currency(@NonNull String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSymbole() {
        return symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    public long getPrix() {
        return prix;
    }

    public void setPrix(long prix) {
        this.prix = prix;
    }

    public boolean isPortefeuille() {
        return portefeuille;
    }

    public void setPortefeuille(boolean portefeuille) {
        this.portefeuille = portefeuille;
    }

    public long getQuantite() {
        return quantite;
    }

    public void setQuantite(long quantite) {
        this.quantite = quantite;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void calculTotal(){
        this.total = this.quantite * this.prix;
    }
}
