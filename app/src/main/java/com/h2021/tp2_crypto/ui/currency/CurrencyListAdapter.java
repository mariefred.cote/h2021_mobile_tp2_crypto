package com.h2021.tp2_crypto.ui.currency;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp2_crypto.R;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;
import com.h2021.tp2_crypto.ui.DialogFragmentCurrency;

import java.util.List;

public class CurrencyListAdapter extends RecyclerView.Adapter<CurrencyViewHolder> {

    private List<Currency> lstCurrency;
    private CurrencyRoomDatabase mDb;
    private Context mContext;
    final String TAG = "tag";

    public void setLstCurrency(List<Currency> lstCurrency) {
        this.lstCurrency = lstCurrency;
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_currency, parent, false);
        mDb = CurrencyRoomDatabase.getDatabase(parent.getContext());
        return new CurrencyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
        mContext = holder.itemView.getContext();
        if (lstCurrency != null){
            Currency current = lstCurrency.get(position);
            holder.tvNomCurrency.setText(current.getNom());
            holder.tvSymboleCurrency.setText(current.getSymbole());
            holder.tvPrixCurrency.setText(String.valueOf(current.getPrix())+ "$");
            int idDarwable = obtenirLogo(current.getSymbole().toLowerCase());
            holder.ivLogoCurrency.setImageResource(idDarwable);
        } else {
            holder.tvNomCurrency.setText("Vide");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb = CurrencyRoomDatabase.getDatabase(mContext);

                if (!lstCurrency.get(position).isPortefeuille()) {
                    lstCurrency.get(position).setPortefeuille(true);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            mDb.currencyDao().updatePortefeuille(true, lstCurrency.get(position).getId());
                        }

                    });
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(mContext,R.string.deja_wallet,Toast.LENGTH_SHORT).show();
                }

            }
        });

        // option de long click
        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuItem modifier = menu.add(0, v.getId(),0, R.string.modifier);
                MenuItem supprimer = menu.add(0, v.getId(),1, R.string.supprimer);

                modifier.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        DialogFragmentCurrency newFragment = new DialogFragmentCurrency(lstCurrency.get(position),lstCurrency.get(position).getId());
                        newFragment.show(((FragmentActivity)mContext).getSupportFragmentManager(),TAG);
                        return false;
                    }
                });

                supprimer.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb.currencyDao().deleteCurrency(lstCurrency.get(position).getId());
                            }
                        });
                        return false;
                    }
                });
            }
        });



    }

    public int obtenirLogo(String symbole) {
        int idDrawable = mContext.getResources().getIdentifier(symbole,"drawable", mContext.getPackageName());
        return idDrawable;
    }

    @Override
    public int getItemCount() {
        if (lstCurrency != null){
            return lstCurrency.size();
        } else {
            return 0;
        }
    }

}


