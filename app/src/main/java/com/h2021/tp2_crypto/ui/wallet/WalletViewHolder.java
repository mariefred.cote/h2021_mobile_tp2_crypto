package com.h2021.tp2_crypto.ui.wallet;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.h2021.tp2_crypto.R;

public class WalletViewHolder extends RecyclerView.ViewHolder {

    public TextView tvNomWallet, tvSymboleWallet, tvPrixWallet, tvTotalWallet, tvQaWallet;
    public CardView cardViewWallet;
    public ImageView ivLogoWallet;


    public WalletViewHolder(@NonNull View itemView) {
        super(itemView);
        tvNomWallet = itemView.findViewById(R.id.tv_nom_wallet);
        tvSymboleWallet = itemView.findViewById(R.id.tv_symboleWallet);
        tvTotalWallet = itemView.findViewById(R.id.tv_totalWallet);
        tvQaWallet = itemView.findViewById(R.id.tv_quantiteWallet);
        tvPrixWallet = itemView.findViewById(R.id.tv_prixWalet);
        cardViewWallet = itemView.findViewById(R.id.cv_wallet);
        ivLogoWallet = itemView.findViewById(R.id.iv_logoWallet);
    }
}
