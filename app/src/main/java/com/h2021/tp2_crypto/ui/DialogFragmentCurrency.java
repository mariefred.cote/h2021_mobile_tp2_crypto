package com.h2021.tp2_crypto.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.h2021.tp2_crypto.R;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;

public class DialogFragmentCurrency extends DialogFragment {

    private Currency currency;
    private int position = -1;
    private CurrencyRoomDatabase mDb;

    public interface EditCurrencyDialogListener {
        void onFinishEditDialog(Currency currency, int position);
    }

    public DialogFragmentCurrency() {
        mDb = CurrencyRoomDatabase.getDatabase(getContext());
    }

    public DialogFragmentCurrency(Currency currency, int position) {
        mDb = CurrencyRoomDatabase.getDatabase(getContext());
        this.currency = currency;
        this.position = position;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.add_currency, null);
        final Context context = getContext();

        if (currency != null){
            EditText etNom = view.findViewById(R.id.et_nom);
            EditText etSymbol = view.findViewById(R.id.et_symbol);
            EditText etPrix = view.findViewById(R.id.et_prix);
            EditText etQuantite = view.findViewById(R.id.et_quantite);

            etNom.setText(currency.getNom());
            etSymbol.setText(currency.getSymbole());
            etPrix.setText(String.valueOf(currency.getPrix()));
            etQuantite.setText(String.valueOf(currency.getQuantite()));
        }

        builder.setView(view).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText etNom = view.findViewById(R.id.et_nom);
                EditText etSymbol = view.findViewById(R.id.et_symbol);
                EditText etPrix = view.findViewById(R.id.et_prix);
                EditText etQuantite = view.findViewById(R.id.et_quantite);

                String nom = etNom.getText().toString().trim();
                String symbol = etSymbol.getText().toString().trim();
                String prix = etPrix.getText().toString().trim();
                String quantite = etQuantite.getText().toString().trim();

                if (nom.isEmpty() || symbol.isEmpty() || prix.isEmpty() || quantite.isEmpty()){
                    Toast.makeText(context,R.string.erreur_dialoge,Toast.LENGTH_LONG).show();
                    return;
                }

                Currency currency = new Currency(nom);
                currency.setSymbole(symbol);
                currency.setPrix(Long.parseLong(prix)/100);
                currency.setQuantite(Long.parseLong(quantite));
                currency.calculTotal();

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (position > -1){
                            mDb.currencyDao().updateCurrency(currency.getNom(), currency.getSymbole(), currency.getPrix(), currency.getQuantite(),currency.getTotal(), position);
                        } else {
                            mDb.currencyDao().insert(currency);
                        }
                    }
                });



            }
        })
        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Cancel par l'utilisateur
            }
        });

        if (position > -1) {
            builder.setTitle(R.string.modifier_currency);
        } else  {
            builder.setTitle(R.string.ajout_currency);
        }


        return builder.create();
    }
}
