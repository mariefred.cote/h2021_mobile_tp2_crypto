package com.h2021.tp2_crypto.ui.wallet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp2_crypto.R;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.model.Currency;
import com.h2021.tp2_crypto.ui.currency.CurrencyViewModel;

import java.util.List;

public class WalletFragment extends Fragment {

    private WalletViewModel walletViewModel;
    private CurrencyViewModel currencyViewModel;
    private RecyclerView recyclerView;
    private TextView tvTotalUser;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        walletViewModel =
                new ViewModelProvider(requireActivity()).get(WalletViewModel.class);
        currencyViewModel =
                new ViewModelProvider(requireActivity()).get(CurrencyViewModel.class);
        View root = inflater.inflate(R.layout.fragment_wallet, container, false);
        tvTotalUser = root.findViewById(R.id.tv_total_user);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_wallet);
        recyclerView = view.findViewById(R.id.rv_wallet);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        WalletListAdapter walletListAdapter = new WalletListAdapter();
        recyclerView.setAdapter(walletListAdapter);



        walletViewModel.getLstCurrency().observe(getViewLifecycleOwner(), new Observer<List<Currency>>() {
            @Override
            public void onChanged(List<Currency> currencies) {
                walletListAdapter.setCurrencyList(currencies);
                tvTotalUser.setText(walletViewModel.getmTotal().getValue());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                walletListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });

    }
}