package com.h2021.tp2_crypto.ui.currency;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp2_crypto.R;

public class CurrencyViewHolder extends RecyclerView.ViewHolder {
    public TextView tvNomCurrency, tvSymboleCurrency, tvPrixCurrency;
    public CardView currencyCardView;
    public ImageView ivLogoCurrency;

    public CurrencyViewHolder(@NonNull View itemView) {
        super(itemView);
        tvNomCurrency = itemView.findViewById(R.id.tv_nomCurrecy);
        tvSymboleCurrency = itemView.findViewById(R.id.tv_symboleCurrency);
        tvPrixCurrency = itemView.findViewById(R.id.tv_prixCurrency);
        currencyCardView = itemView.findViewById(R.id.cv_currency);
        ivLogoCurrency = itemView.findViewById(R.id.iv_logoCurrency);

    }

}

