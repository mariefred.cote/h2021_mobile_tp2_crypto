package com.h2021.tp2_crypto.ui.wallet;

import android.content.Context;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp2_crypto.R;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;
import com.h2021.tp2_crypto.ui.DialogFragmentCurrency;

import java.util.List;

public class WalletListAdapter extends RecyclerView.Adapter<WalletViewHolder> {

    private List<Currency> currencyList;
    private CurrencyRoomDatabase mDb;
    private Context mContext;
    final String TAG = "tag" ;

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }


    @NonNull
    @Override
    public WalletViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_wallet, parent, false);
        mDb = CurrencyRoomDatabase.getDatabase(parent.getContext());
        return new WalletViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletViewHolder holder, int position) {
        mContext = holder.itemView.getContext();
        if (currencyList != null) {
            Currency current = currencyList.get(position);
            holder.tvNomWallet.setText(current.getNom());
            holder.tvSymboleWallet.setText(current.getSymbole());
            holder.tvQaWallet.setText(String.valueOf(current.getQuantite()));
            holder.tvPrixWallet.setText(String.valueOf(current.getPrix()));
            holder.tvTotalWallet.setText(String.valueOf(current.getTotal()) + "$");
            int idDarwable = obtenirLogo(current.getSymbole().toLowerCase());
            holder.ivLogoWallet.setImageResource(idDarwable);
        }

        // option de long click
        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuItem modifier = menu.add(0, v.getId(),0, R.string.modifier);
                MenuItem supprimer = menu.add(0, v.getId(),1, R.string.supprimer);

                modifier.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        DialogFragmentCurrency newFragment = new DialogFragmentCurrency(currencyList.get(position),currencyList.get(position).getId());
                        newFragment.show(((FragmentActivity)mContext).getSupportFragmentManager(),TAG );
                        return false;
                    }
                });

                supprimer.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        currencyList.get(position).setPortefeuille(false);
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb.currencyDao().updatePortefeuille(false, currencyList.get(position).getId());
                            }
                        });
                        return false;
                    }
                });
            }
        });

    }

    // Obtenir el bon logo avec le symbol
    public int obtenirLogo(String symbole) {
        int idDrawable = mContext.getResources().getIdentifier(symbole,"drawable", mContext.getPackageName());
        return idDrawable;
    }

    @Override
    public int getItemCount() {
        if (currencyList != null){
            return currencyList.size();
        } else {
            return 0;
        }
    }
}
