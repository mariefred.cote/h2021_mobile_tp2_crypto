package com.h2021.tp2_crypto.ui.currency;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;

import java.util.List;

public class CurrencyViewModel extends AndroidViewModel {

    private  LiveData<List<Currency>> lstCurrency;
    private CurrencyRoomDatabase mDb;
    private MutableLiveData<Boolean> estAdmin;

    public CurrencyViewModel(Application application) {
        super(application);
        mDb = CurrencyRoomDatabase.getDatabase(application);
        lstCurrency =mDb.currencyDao().getAllCurrency();
        estAdmin =  new MutableLiveData<>();
        estAdmin.setValue(false);

    }

    public void setEstAdmin(MutableLiveData<Boolean> estAdmin) {
        this.estAdmin = estAdmin;
    }

    public MutableLiveData<Boolean> getEstAdmin() {
        return estAdmin;
    }

    public LiveData<List<Currency>> getLstCurrency() {
        return lstCurrency;
    }


}