package com.h2021.tp2_crypto.ui.currency;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.h2021.tp2_crypto.R;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;
import com.h2021.tp2_crypto.ui.DialogFragmentCurrency;

import java.util.List;

public class CurrencyFragment extends Fragment {

    private CurrencyViewModel currencyViewModel;
    private CurrencyRoomDatabase mDb;
    private FloatingActionButton btAdd;
    private CurrencyListAdapter adapter;
    private RecyclerView recyclerView;
    final String TAG = "tag";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        currencyViewModel = new ViewModelProvider(requireActivity()).get(CurrencyViewModel.class);
        View root = inflater.inflate(R.layout.fragment_currency, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_currency);
        recyclerView = view.findViewById(R.id.rv_currency);

        mDb = CurrencyRoomDatabase.getDatabase(getContext());
        btAdd = view.findViewById(R.id.fa_add);
        btAdd.setVisibility(View.INVISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        CurrencyListAdapter currencyListAdapter = new CurrencyListAdapter();
        recyclerView.setAdapter(currencyListAdapter);

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragmentCurrency newFragment = new DialogFragmentCurrency();
                newFragment.show(getChildFragmentManager(), TAG);

            }
        });

        currencyViewModel.getEstAdmin().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean){
                    btAdd.setVisibility(View.VISIBLE);
                } else  {
                    btAdd.setVisibility(View.INVISIBLE);
                }
            }
        });


        currencyViewModel.getLstCurrency().observe(getViewLifecycleOwner(), new Observer<List<Currency>>() {
            @Override
            public void onChanged(List<Currency> currencies) {
                currencyListAdapter.setLstCurrency(currencies);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currencyListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });


            }
        });

    }
}