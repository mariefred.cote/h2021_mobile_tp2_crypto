package com.h2021.tp2_crypto.ui.wallet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;

import java.util.List;

public class WalletViewModel extends AndroidViewModel {

    private MutableLiveData<String> mTotal;
    private  LiveData<List<Currency>> lstCurrency;
    private CurrencyRoomDatabase mDb;


    public WalletViewModel(Application application) {
        super(application);
        mDb = CurrencyRoomDatabase.getDatabase(application);
        lstCurrency =mDb.currencyDao().getAllWallet(true);
        mTotal = new MutableLiveData<>();

    }

    public LiveData<List<Currency>> getLstCurrency() {
        return lstCurrency;
    }

    public MutableLiveData<String> getmTotal() {
        long total = 0;
        if (lstCurrency.getValue() != null){
            for (Currency currency: lstCurrency.getValue())
            {
                total += currency.getTotal();
            }
        }
        mTotal.setValue(String.valueOf(total)+"$");
        return mTotal;
    }

}