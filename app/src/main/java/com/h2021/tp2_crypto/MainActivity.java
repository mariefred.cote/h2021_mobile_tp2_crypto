package com.h2021.tp2_crypto;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.h2021.tp2_crypto.data.AppExecutors;
import com.h2021.tp2_crypto.data.CurrencyRoomDatabase;
import com.h2021.tp2_crypto.model.Currency;
import com.h2021.tp2_crypto.ui.currency.CurrencyViewModel;
import com.h2021.tp2_crypto.ui.wallet.WalletViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int CAMERA = 1;
    private CurrencyRoomDatabase mDb;
    Toolbar tbar;
    MutableLiveData<Boolean> estAdmin;
    private CurrencyViewModel currencyViewModel;
    private WalletViewModel walletViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currencyViewModel = new ViewModelProvider(this).get(CurrencyViewModel.class);
        walletViewModel = new ViewModelProvider(this).get(WalletViewModel.class);

        estAdmin = currencyViewModel.getEstAdmin();
        mDb = CurrencyRoomDatabase.getDatabase(getApplicationContext());
        tbar = findViewById(R.id.tbar);
        setSupportActionBar(tbar);
        tbar.setTitle("Crypto");


        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                List<Currency> lstCurrency =  mDb.currencyDao().getCurrencies();
                Log.d("tag", "run: " + lstCurrency.size());

                if (lstCurrency.size() < 1) {
                    //mDb.currencyDao().deleteAll();
                    Currency currency = new Currency("Bitcoin");
                    currency.setSymbole("BTC");
                    currency.setPrix(45000);
                    currency.setQuantite(1);
                    currency.calculTotal();
                    mDb.currencyDao().insert(currency);

                    currency = new Currency("Ethereum");
                    currency.setSymbole("ETH");
                    currency.setPrix(1450);
                    currency.setQuantite(1);
                    currency.calculTotal();
                    mDb.currencyDao().insert(currency);

                }
            }
        });





        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_currency, R.id.navigation_wallet)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_admin:
                if (estAdmin.getValue() == false){
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) !=
                            PackageManager.PERMISSION_GRANTED){
                        requestPermissions(new String[]{Manifest.permission.CAMERA},CAMERA);
                    } else {
                        estAdmin.setValue(true);
                        currencyViewModel.setEstAdmin(estAdmin);
                        Toast.makeText(MainActivity.this, R.string.admin_active,Toast.LENGTH_SHORT).show();
                    }
                } else  {
                    estAdmin.setValue(false);
                    currencyViewModel.setEstAdmin(estAdmin);
                    Toast.makeText(MainActivity.this, R.string.admin_desactive,Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.mn_supp:
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.currencyDao().deleteAll();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == CAMERA){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                estAdmin.setValue(true);
                currencyViewModel.setEstAdmin(estAdmin);
                Toast.makeText(MainActivity.this, R.string.admin_active,Toast.LENGTH_SHORT).show();
            } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.CAMERA)){
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setTitle(R.string.permission_title);
                    dialog.setMessage(R.string.permission_message);
                    dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},CAMERA);
                        }
                    });
                    dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MainActivity.this, R.string.admin_refu,Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog.show();
                }
            }
        }
    }
}